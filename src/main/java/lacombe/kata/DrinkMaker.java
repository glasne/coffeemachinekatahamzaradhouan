package lacombe.kata;

public interface DrinkMaker {
    void makeDrink(String order);
}